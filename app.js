const url = 'https://rickandmortyapi.com/api/character';
fetch(url)
    .then(res => res.json())
    .then(data => makeCards(data.results))

function listEpisodes(n) {
    let list = []
    n.forEach(elem => {
        list.push(Number(elem.replace(/[^0-9\-]+/g, "")))
    })
    return list.join(', ')
}

function makeCards(charactersArray) {
    const cardContainer = document.querySelector('#card-container')
    charactersArray.forEach(character => {
        list = listEpisodes(character.episode)
        cardContainer.innerHTML = cardContainer.innerHTML +
            `<div id='character-card-${character.id}' class='item' tag1='${character.created}' tag2='${character.episode.length}' tag3='${character.name}'>
                <img class='img' src=${character.image}></img>
                <div>
                <div class='name'><b>Name:</b> ${character.name}</div>
                <div><b>Species:</b> ${character.species}</div>
                <div><b>Location</b>: ${character.location.name}</div>
                <div id='${character.created} class='created'><b>Created:</b> ${character.created.slice(0,10)}</div>
                <div><b>Episodes:</b> ${list}</div>
                <button id='delete-button-${character.id}' name='button' onclick="deleteSelf(this)">Delete</button>
                </div>
            </div>`
    })
}

function deleteSelf(button) {
    const elem = document.getElementById(button.id)
    elem.parentElement.parentElement.remove()
}

let counter = 1

window.addEventListener("scroll", function() {

    var block = document.getElementById('infinite-scroll')

    var contentHeight = block.offsetHeight
    var yOffset = window.pageYOffset
    var window_height = window.innerHeight
    var y = yOffset + window_height


    if (y >= contentHeight) {
        counter = counter + 1
        console.log(counter)
        console.log(url + `/?page=${counter}`)
        fetch(url + `/?page=${counter}`)
            .then(res => res.json())
            .then(data => makeCards(data.results))
    }
});

document.querySelector('#sort-asc').onclick = mySortAscDate
document.querySelector('#sort-desc').onclick = mySortDescDate
document.querySelector('#sort-asc-episodes').onclick = mySortAscEpisodes
document.querySelector('#sort-desc-episodes').onclick = mySortDescEpisodes

function mySortAscDate() {
    let card = document.querySelector('#card-container')
    for (let i = 0; i < card.children.length; i++) {
        for (let j = i; j < card.children.length; j++) {
            if (Date.parse(card.children[i].getAttribute('tag1')) > Date.parse(card.children[j].getAttribute('tag1'))) {
                replaceNode = card.replaceChild(card.children[j], card.children[i])
                insertAfter(replaceNode, card.children[i])
            }
        }
    }
}

function mySortDescDate() {
    let card = document.querySelector('#card-container')
    for (let i = 0; i < card.children.length; i++) {
        for (let j = i; j < card.children.length; j++) {
            if (Date.parse(card.children[i].getAttribute('tag1')) < Date.parse(card.children[j].getAttribute('tag1'))) {
                replaceNode = card.replaceChild(card.children[j], card.children[i])
                insertAfter(replaceNode, card.children[i])
            }
        }
    }
}

function insertAfter(elem, refElem) {
    return refElem.parentNode.insertBefore(elem, refElem.nextSibling)
}

function mySortAscEpisodes() {
    let card = document.querySelector('#card-container')
    for (let i = 0; i < card.children.length; i++) {
        for (let j = i; j < card.children.length; j++) {
            if (+card.children[i].getAttribute('tag2') > +card.children[j].getAttribute('tag2')) {
                replaceNode = card.replaceChild(card.children[j], card.children[i])
                insertAfter(replaceNode, card.children[i])
            } else if (+card.children[i].getAttribute('tag2') === +card.children[j].getAttribute('tag2')) {
                mySortAscDate
            }
        }
    }
}

function mySortDescEpisodes() {
    let card = document.querySelector('#card-container')
    for (let i = 0; i < card.children.length; i++) {
        for (let j = i; j < card.children.length; j++) {
            if (+card.children[i].getAttribute('tag2') < +card.children[j].getAttribute('tag2')) {
                replaceNode = card.replaceChild(card.children[j], card.children[i])
                insertAfter(replaceNode, card.children[i])
            } else if (+card.children[i].getAttribute('tag2') === +card.children[j].getAttribute('tag2')) {
                mySortAscDate
            }
        }
    }
}

document.querySelector('#elastic').oninput = function() {
    let val = this.value.trim()
    let elasticItems = document.querySelector('#card-container').children
    if (val != '') {
        for (let i = 0; i < elasticItems.length; i++) {
            console.log(elasticItems[i].getAttribute('tag3'))
            if (elasticItems[i].getAttribute('tag3').search(val) == -1) {
                elasticItems[i].classList.add('hide')
            } else {
                elasticItems[i].classList.remove('hide')
            }
        }
    } else {
        for (let i = 0; i < elasticItems.length; i++) {
            elasticItems[i].classList.remove('hide')
        }
    }
}